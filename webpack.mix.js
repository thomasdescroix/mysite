let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/script.js', 'public/js')
    .js('resources/assets/js/scrumboard.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/guest.scss', 'public/css')
    .sass('resources/assets/sass/auth.scss', 'public/css')
    .copy('node_modules/materialize-css/dist/css/materialize.css', 'public/css/materialize.css')
    .copy('node_modules/materialize-css/dist/js/materialize.js', 'public/js/materialize.js');




