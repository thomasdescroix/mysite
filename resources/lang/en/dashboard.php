<?php

return [

    'view_more'          =>  'View more',
    'project_count'      =>  ':count projects',
    'user_count'         =>  ':count users'

];
