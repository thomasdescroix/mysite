<?php

return [

    'success' => [
        'store'     => 'New project created!',
        'update'    => 'Project updated!',
        'destroy'   => 'Project deleted!'
    ],
    'warning' => [
        'show'      => 'This sprint doesn\'t exist!'
    ]

];
