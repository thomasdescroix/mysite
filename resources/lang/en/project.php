<?php

return [

    'success'   =>  [
        'store'     =>  'New project created!',
        'update'    =>  'Project updated!',
        'destroy'   =>  'Project deleted!'
    ],
    'title' =>  [
        'create'    =>  'Create new project',
        'edit'      =>  'Edit :Project'
    ],
    'form'  =>  [
        'label' =>  [
            'name'          =>  'Title',
            'status'        =>  'Status',
            'description'   =>  'Description'
        ],
        'submit'    =>  [
            'create'    =>  'Create :icon',
            'edit'      =>  'Edit :icon'
        ],
        'help'  =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ],
    'index' =>  [
        'table' =>  [
            'name'          =>  'Name',
            'status'        =>  'Status',
            'sprint'        =>  'Sprint',
            'spent_hours'   =>  'Spent Hours',
            'action'        =>  'Action'
        ],
        'destroy_confirmation'  =>  'Are you sure you want to delete this project ?'
    ]

];
