function redirectToSprint() {
    const option = document.getElementById("sprint").value;
    location.replace("/projects/" + this + /sprints/ + option);
}

document.addEventListener("DOMContentLoaded", function() {
    // redirect to another sprint number
    const project = document.getElementById("sprint").getAttribute("data-id");
    document.getElementById("sprint").addEventListener("change", redirectToSprint.bind(project));

    let dragged;
    let cards;

    document.addEventListener("dragstart", function( event ) {
        // store a ref. on the dragged elem
        dragged = event.target;
        cards = document.getElementsByClassName("card-task");
        for (let card of cards) {
            if (card !== dragged)
                card.style.pointerEvents = "none";
        }
        // make it half transparent
        event.target.style.opacity = .5;
    }, false);

    document.addEventListener("dragend", function( event ) {
        // reset the transparency
        event.target.style.opacity = "";
    }, false);

    /* events fired on the drop targets */
    document.addEventListener("dragover", function( event ) {
        // prevent default to allow drop
        event.preventDefault();
    }, false);

    document.addEventListener("dragenter", function( event ) {
        // highlight potential drop target when the draggable element enters it
        if ( event.target.classList.contains("dropzone")) {
            event.target.style.background = "purple";
        }

    }, false);

    document.addEventListener("dragleave", function( event ) {
        // reset background of potential drop target when the draggable element leaves it
        if ( event.target.classList.contains("dropzone")) {
            event.target.style.background = "";
        }

    }, false);

    document.addEventListener("drop", function( event ) {
        // prevent default action (open as link for some elements)
        event.preventDefault();
        // move dragged elem to the selected drop target
        if ( event.target.classList.contains("dropzone")) {
            for (let card of cards) {
                if (card !== dragged)
                    card.style.pointerEvents = "auto";
            }
            event.target.style.background = "";
            dragged.parentNode.removeChild( dragged );
            event.target.appendChild( dragged );

            const xmlhttp = new XMLHttpRequest();
            const status = event.target.dataset.title;
            const task = dragged.dataset.id;

            xmlhttp.open("PUT", `/tasks/${task}/status/${status}`);
            xmlhttp.setRequestHeader('X-CSRF-TOKEN', document.head.querySelector("meta[name=csrf-token]").content);
            xmlhttp.onload = function() {
                if(this.readyState === 4 && this.status === 200) {
                    console.log('ok');
                } else {
                    console.log('Request failed!');
                }
            };
            xmlhttp.send();
        }
    }, false);


});

