document.addEventListener('DOMContentLoaded', function() {
    let options = {};

    const sidenavElem = document.querySelector('.sidenav');
    M.Sidenav.init(sidenavElem, options);

    const collapsibleElems = document.querySelectorAll('.collapsible');
    M.Collapsible.init(collapsibleElems, options);

    const tabsElems = document.querySelectorAll('.tabs');
    M.Tabs.init(tabsElems, options);

    const FloatingBtnElems = document.querySelectorAll('.fixed-action-btn');
    M.FloatingActionButton.init(FloatingBtnElems, options);

    const FormSelectElems = document.querySelectorAll('select');
    M.FormSelect.init(FormSelectElems, options);

    options = { opacity: 0.7 };
    const ModalElems = document.querySelectorAll('.modal');
    M.Modal.init(ModalElems, options);
});
