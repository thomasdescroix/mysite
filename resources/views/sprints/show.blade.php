@extends('layouts.app')

@section('title')
    Projects
@endsection

@section('content')
    <div class="input-field">
        <select data-id="{{ $project->id }}" id="sprint">
            <option value="" disabled>Choose your sprint</option>
            @foreach ($project->sprints as $s)
                <option value="{{ $s->id }}" @if ($sprint->id === $s->id) selected @endif >Sprint {{ $s->number }}</option>
            @endforeach
        </select>
        <label for="sprint">Choose your sprint</label>
    </div>
    <div class="scrumboard">
        <div class="d-flex">
            <!-- TODO: we want to administrate the scrum board list -->
            @foreach($tasksByStatus as $key => $tasks)
                <div class="card card-list">
                    <div class="card-title teal lighten-1 white-text">{{ $key }}</div>
                    <div class="card-content dropzone" data-title="{{ $key }}">
                        @foreach($tasks as $task)
                            <div class="card card-task" draggable="true" data-id="{{ $task->id }}">
                                <div class="card-content">
                                    <p>{{ $task->name }}</p>
                                </div>
                                <div class="card-action teal lighten-5 d-flex justify-content-between align-items-center">
                                    <span class="">12/{{ $task->id }} -
                                        @if($task->user)
                                            {{ $task->user()->value('name') }}
                                        @else
                                            Nobody
                                        @endif
                                    </span>
                                    <a role="button" class="activator mr-0 teal-text">
                                        <i class="material-icons tiny">settings</i>
                                    </a>
                                </div>
                                <!-- TODO: redo a dropdown -->
                                <div class="card-reveal p-0">
                                    <button class="card-title waves-effect waves-light btn-small teal lighten-5 mb-0 w-100"><i class="material-icons tiny teal-text">arrow_drop_down</i></button>
                                    <div class="collection m-0">
                                        <a href="#!" class="collection-item">Log activity</a>
                                        <a href="{{ route('projects.sprints.tasks.edit', [$project, $sprint, 144]) }}" class="collection-item">Edit</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/scrumboard.js') }}" defer></script>
@endpush