@extends('layouts.app')

@section('content')

    <div class="d-flex align-items-center justify-content-center flex-column">

        <!-- TODO: Test the displaying of the error message, improve the design, add close btn -->
        @if (session('danger'))
            <div class="card-panel red darken-1 white-text alert" aria-label="Alert">
                <p>{{ session('danger') }}</p>
                <a href="{{ route('auth.resend', encrypt(old('email'))) }}">Resend the verification mail.</a>
            </div>
        @endif

        <div class="card @empty($errors->any()) card-zoom @endempty">
            <div class="card-content">

                <h1 class="card-title center-align">{{ __('Login') }}</h1>
                <div class="divider"></div>

                {{ Form::open(array('route' => 'login', 'aria-label' => 'Login')) }}

                    <div class="input-field">
                        {{ Form::label('email', __('E-mail Address')) }}
                        {{ Form::email('email', null, ['class' => $errors->has('email') ? 'invalid' : '']) }}
                        @if ($errors->has('email'))
                            <span class="helper-text" data-error="{{ $errors->first('email') }}" role="alert">Helper text for email</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('password', __('Password')) }}
                        {{ Form::password('password', array('class' => $errors->has('password') ? 'invalid' : '')) }}
                        @if ($errors->has('password'))
                            <span class="helper-text" data-error="{{ $errors->first('password') }}" role="alert">Helper text</span>
                        @endif
                    </div>

                    <div class="row">
                        <label>
                            {{ Form::checkbox('remember', '', old('remember') ? true : false) }}
                            <span>{{ __('Remember Me') }}</span>
                        </label>
                    </div>

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="waves-effect waves-light btn teal lighten-1" >
                            {{ __('Login') }}
                        </button>
                    </div>
                {{ Form::close() }}

            </div>
            <div class="card-action">
                <a class="teal-text" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                <a class="teal-text" href="{{ route('register') }}">{{ __('Register') }}</a>
            </div>

        </div>
    </div>

@endsection
