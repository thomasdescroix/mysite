@extends('layouts.app')

@section('content')

    <div class="d-flex align-items-m-center justify-content-center flex-column">

        <div class="card @empty($errors->any()) card-zoom @endempty">
            <div class="card-content">

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <h1 class="card-title center-align">{{ __('Reset Password') }}</h1>
                <div class="divider"></div>

                {{ Form::open(array('route' => 'password.email', 'aria-label' => 'Reset Password')) }}

                    <div class="input-field">
                        {{ Form::label('email', __('E-mail Address')) }}
                        {{ Form::email('email', null, array('class' => $errors->has('email') ? 'invalid' : '')) }}
                        @if ($errors->has('email'))
                            <span class="helper-text" data-error="{{ $errors->first('email') }}" role="alert">Helper text for email</span>
                        @endif
                    </div>

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="waves-effect waves-light btn teal lighten-1" >
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>

                {{ Form::close() }}

            </div>
            <div class="card-action">
                <a class="teal-text" href="{{ route('login') }}">{{ __('Login') }}</a>
                <a class="teal-text" href="{{ route('register') }}">{{ __('Register') }}</a>
            </div>

        </div>
    </div>
@endsection
