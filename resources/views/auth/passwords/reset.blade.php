@extends('layouts.app')

@section('content')
    <!-- TODO: test / maybe we can rebuild the view because it's the password's reset of logged user -->
    <div class="d-flex align-items-m-center justify-content-center flex-column">

        <div class="card @empty($errors->any()) card-zoom @endempty">
            <div class="card-content">

                <h1 class="card-title center-align">{{ __('Reset Password') }}</h1>
                <div class="divider"></div>

                {{ Form::open(array('route' => 'password.request', 'aria-label' => 'Reset Password')) }}

                    {{ Form::hidden('token', $token) }} <!-- TODO: test it -->

                    <div class="input-field">
                        {{ Form::label('email', __('E-mail Address')) }}
                        {{ Form::email('email', null, array('class' => $errors->has('email') ? 'invalid' : '')) }}
                        @if ($errors->has('email'))
                            <span class="helper-text" data-error="{{ $errors->first('email') }}" role="alert">Helper text for email</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('password', __('Password')) }}
                        {{ Form::password('password', array('class' => $errors->has('password') ? 'invalid' : '')) }}
                        @if ($errors->has('password'))
                            <span class="helper-text" data-error="{{ $errors->first('password') }}" role="alert">Helper text</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('password_confirmation', __('Confirm Password')) }}
                        {{ Form::password('password_confirmation', array('class' => $errors->has('password_confirmation') ? 'invalid' : '')) }}
                    </div>

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="waves-effect waves-light btn teal lighten-1" >
                            {{ __('Reset Password') }}
                        </button>
                    </div>

                {{ Form::close() }}

            </div>

        </div>
    </div>
@endsection
