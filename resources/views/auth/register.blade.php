@extends('layouts.app')

@section('content')

    <div class="d-flex align-items-m-center justify-content-center flex-column">

        <div class="card @empty($errors->any()) card-zoom @endempty">
            <div class="card-content">

                <h1 class="card-title center-align">{{ __('Register') }}</h1>
                <div class="divider"></div>

                {{ Form::open(array('route' => 'register', 'aria-label' => 'Register')) }}

                    <div class="input-field">
                        {{ Form::label('name', __('Name')) }}
                        {{ Form::text('name', null, array('class' => $errors->has('name') ? 'invalid' : '')) }}
                        @if ($errors->has('name'))
                            <span class="helper-text" data-error="{{ $errors->first('name') }}" role="alert">Helper text for name</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('email', __('E-mail Address')) }}
                        {{ Form::email('email', null, array('class' => $errors->has('email') ? 'invalid' : '')) }}
                        @if ($errors->has('email'))
                            <span class="helper-text" data-error="{{ $errors->first('email') }}" role="alert">Helper text for email</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('password', __('Password')) }}
                        {{ Form::password('password', array('class' => $errors->has('password') ? 'invalid' : '')) }}
                        @if ($errors->has('password'))
                            <span class="helper-text" data-error="{{ $errors->first('password') }}" role="alert">Helper text</span>
                        @endif
                    </div>

                    <div class="input-field">
                        {{ Form::label('password_confirmation', __('Confirm Password')) }}
                        {{ Form::password('password_confirmation', array('class' => $errors->has('password_confirmation') ? 'invalid' : '')) }}
                    </div>

                    <div class="d-flex justify-content-center">
                        <button type="submit" class="waves-effect waves-light btn teal lighten-1" >
                            {{ __('Register') }}
                        </button>
                    </div>

                {{ Form::close() }}

            </div>
            <div class="card-action">
                <a class="teal-text" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                <a class="teal-text" href="{{ route('login') }}">{{ __('Login') }}</a>
            </div>

        </div>
    </div>

@endsection
