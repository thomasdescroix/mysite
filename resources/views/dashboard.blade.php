@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col s6">
            <div class="card hoverable">
                <div class="card-content">
                    <span class="card-title"><i class="tiny material-icons">work</i>&nbsp;&nbsp;{{ __('dashboard.project_count', ['count' => $nbProjects]) }}</span>
                </div>
                <div class="card-action">
                    <a class="teal-text text-lighten-1" href="{{ route('projects.index') }}">{{ __('dashboard.view_more') }}</a>
                </div>
            </div>
        </div>
        <div class="col s6">
            <div class="card hoverable">
                <div class="card-content">
                    <span class="card-title"><i class="tiny material-icons">group</i>&nbsp;&nbsp;{{ __('dashboard.user_count', ['count' => $nbUsers]) }}</span>
                </div>
                <div class="card-action">
                    <a class="teal-text text-lighten-1" href="#">{{ __('dashboard.view_more') }}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Team -->
        <div class="col s12 m6">
            <div class="card hoverable">
                <div class="card-tabs">
                    <ul class="tabs tabs-fixed-width">
                        @foreach($teams as $team)
                            <li class="tab col"><a href="#team-{{ $loop->index + 1 }}">{{ $team->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="card-content p-0">
                    @foreach($teams as $team)
                        <div id="team-{{ $loop->index + 1 }}">
                            <ul class="collection mt-0">
                                @foreach($team->users as $u)
                                    <li class="collection-item avatar">
                                        <img class="circle" src="{{ $u->avatar }}" alt="User image">
                                        <span class="title">{{ $u->name }}</span>
                                        <p>{{ $u->email }}</p>
                                        <a href="#" class="secondary-content">
                                            <i class="material-icons">send</i>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- /Team -->
    </div>
@endsection

