<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com" />
    @stack('styles')
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.css') }}"  media="screen,projection" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    @guest
        <link rel="stylesheet" href="{{ asset('css/guest.css') }}" />
    @endguest
    @auth
        <link rel="stylesheet" href="{{ asset('css/auth.css') }}" />
    @endauth
</head>
<body>
    <div id="app">
        @auth
            <!-- sidenav -->
            <ul id="slide-out" class="sidenav sidenav-fixed" role="menu">
                <li>
                    <div class="user-view mb-0">
                        <div class="background teal lighten-1">
                            <img src="{{ asset('images/user-background.jpg') }}" alt="Background user view">
                        </div>
                        <img class="circle" src="{{ Auth::user()->avatar }}" alt="User image">
                        <span class="white-text name">{{ Auth::user()->name }}</span>
                        <span class="white-text email">{{ Auth::user()->email }}</span>

                    </div>
                </li>
                <li><a class="waves-effect waves-light {{ Request::is('/') ? 'white-text teal lighten-1' : null }}" href="{{ route('dashboard') }}"><i class="material-icons {{ Request::is('/') ? 'white-text' : null }}">dashboard</i>{{ __('sidenav.dashboard') }}</a></li>
                <li><a class="waves-effect waves-light {{ Request::is('projects*') ? 'white-text teal lighten-1' : null }}" href="{{ route('projects.index') }}"><i class="material-icons {{ Request::is('projects*') ? 'white-text' : null }}">work</i>{{ __('sidenav.project') }}</a></li>
            </ul>
            <!-- top navbar -->
            <nav class="teal lighten-1 mb-3">
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <div class="nav-wrapper container-fluid">
                    <div class="col s12">
                        @yield('title') <!-- TODO: breadcrumbs of laravel + show title -->
                        <div class="right pr-4">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="material-icons">power_settings_new</i>
                            </a>
                            <!-- TODO: use Form -->
                            <form method="post" action="{{ route('logout') }}" id="logout-form">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
        @endauth

        <!-- Page content -->
        <main>
            @yield('content')
        </main>
    </div>

    <script src="{{ asset('js/materialize.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <script defer>
        document.addEventListener('DOMContentLoaded', function() {
            @if (session()->has('warning'))
                var toastHTML = "<span role='alert'>{{ session('warning') }}</span>";
                M.toast({html: toastHTML, classes: 'yellow lighten-4 orange-text'});
            @elseif (session()->has('success'))
                var toastHTML = "<span role='alert'>{{ session('success') }}</span>";
                M.toast({html: toastHTML, classes: 'light-green  lighten-4 green-text'});
            @endif
        });
    </script>
    @stack('scripts')
</body>
</html>
