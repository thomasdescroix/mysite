@extends('layouts.app')

@section('title')
    Projects
@endsection

@section('content')
    @foreach ($errors->all() as $error)
        <span class="alert hide" role="alert">{{ $error }}</span>
    @endforeach
    <div class="card card-form">
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">{{ __('team.form.title', ['project' => $project->name ]) }}<i class="material-icons right">help_outline</i></span>
            {{ Form::open(['route' => ['projects.users.update', $project ], 'method' => 'put']) }}
                <div class="row">
                    <div class="input-field col s12">
                        <select id="users" name="users[]" multiple="multiple" class="icons">
                            <option value="" disabled>Choose your users</option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}" @if(in_array($user->id, $team_members)) selected @endif data-icon="{{ $user->avatar }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        <label for="users">{{ __('team.form.label.users') }}</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col right">
                        {{ Form::button(__('team.form.submit.edit', ['icon' => '<i class="material-icons right">mode_edit</i>']), ['type' => 'submit', 'class' => 'btn waves-effect btn teal lighten-1 hoverable']) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <!-- TODO: reveal add information -->
        <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">{{ __('team.form.title', ['project' => $project->name ]) }}<i class="material-icons right">close</i></span>
            <p>{{ __('team.form.help') }}</p>
        </div>
    </div>
    @if(!empty($team_members))
        <ul class="collection hoverable container">
            @foreach($users as $user)
                @if(in_array($user->id, $team_members))
                    <li class="collection-item avatar">
                        <img src="{{ $user->avatar }}" alt="avatar" class="circle">
                        <span class="title">{{ $user->name }}</span>
                        <p>{{ $user->email }}</p>
                    </li>
                @endif
            @endforeach
        </ul>
    @endif

@endsection

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const alertElems = document.querySelectorAll("span.alert");
            alertElems.forEach(function(e){
                M.toast({ html: e.innerHTML, classes: 'red darken-1', displayLength: 5000 });
            });
        });
    </script>
@endpush