@extends('layouts.app')

@section('title')
    Projects
@endsection

@section('content')
    @foreach ($errors->all() as $error)
        <span class="alert hide" role="alert">{{ $error }}</span>
    @endforeach
    <div class="card card-form">
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">{{ __('project.title.create') }}<i class="material-icons right">help_outline</i></span>
            {{ Form::open(['route' => 'projects.store']) }}
                <div class="row">
                    <div class="input-field col s12 m6">
                        {{ Form::label('name', __('project.form.label.name')) }}
                        {{ Form::text('name', null, ['data-length' => '60']) }}
                    </div>
                    <div class="input-field col s12 m6">
                        {{ Form::select('status', ['New' => 'New', 'In Progress' => 'In Progress', 'Done' => 'Done'], 'New') }}
                        {{ Form::label('status', __('project.form.label.status')) }}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        {{ Form::label('description', __('project.form.label.description')) }}
                        {{ Form::textarea('description', null, ['data-length' => '600', 'class' => 'materialize-textarea']) }}

                    </div>
                </div>
                <div class="row">
                    <div class="col right">
                        {{ Form::button(__('project.form.submit.create', ['icon' => '<i class="material-icons right">add</i>']), ['type' => 'submit', 'class' => 'btn waves-effect btn teal lighten-1 hoverable']) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <!-- TODO: reveal add information -->
        <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">{{ __('project.title.create') }}<i class="material-icons right">close</i></span>
            <p>{{ __('project.form.help') }}</p>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const alertElems = document.querySelectorAll("span.alert");
            alertElems.forEach(function(e){
                M.toast({ html: e.innerHTML, classes: 'red darken-1', displayLength: 5000 });
            });

            const inputElems = document.querySelectorAll("input[name='name'], textarea[name='description']");
            M.CharacterCounter.init(inputElems);
        });
    </script>
@endpush