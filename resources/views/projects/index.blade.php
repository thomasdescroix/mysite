@extends('layouts.app')

@section('title')
    Projects
@endsection

@section('content')
    <!-- TODO: use dataTables -->
    <div class="d-flex justify-content-center" aria-label="pagination">
        {{ $pages }}
    </div>

    <table class="highlight responsive-table" id="table-projects">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('project.index.table.name') }}</th>
                <th>{{ __('project.index.table.status') }}</th>
                <th class="center">{{ __('project.index.table.sprint') }}</th>
                <th class="center">{{ __('project.index.table.spent_hours') }}</th>
                @if(auth()->user()->is_admin)
                    <th class="center-align">{{ __('project.index.table.action') }}</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{ $loop-> index + 1 }}</td>
                    <td><a href="{{ route('projects.sprints.show', [$project->id, $project->sprints->last()]) }}">{{ $project->name }}</a></td>
                    <td>{{ $project->status }}</td>
                    <td class="center">{{ $project->sprints->last()->number }}</td>
                    <td class="center"></td>
                    @if(auth()->user()->is_admin)
                        <td class="center-align">
                            <!-- Modal Trigger -->
                            <button data-target="modal-confirmation-{{ $loop->index + 1 }}" class="waves-effect btn teal lighten-1 modal-trigger" data-toggle="modal">
                                <i class="material-icons">delete_forever</i>
                            </button>
                            <form method="post" action="{{ route('projects.destroy', $project->id) }}" class="hide" id="project-delete-{{ $loop->index + 1 }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <a class="waves-effect btn teal lighten-1" href="{{ route('projects.edit', $project) }}">
                                <i class="material-icons">mode_edit</i>
                            </a>
                            <a class="waves-effect btn teal lighten-1" href="{{ route('projects.users.edit', $project) }}">
                                <i class="material-icons">group_add</i>
                            </a>
                        </td>
                    @endif
                </tr>
                <!-- TODO: use just one modal -->
                <!-- Modal Structure -->
                <div id="modal-confirmation-{{ $loop->index + 1 }}" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-content">
                        <p>{{ __('project.index.destroy_confirmation') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button class="waves-effect waves-green btn green" onclick="event.preventDefault(); document.getElementById('project-delete-{{ $loop->index + 1 }}').submit();">
                            <i class="material-icons">check</i>
                        </button>
                        <button class="modal-close waves-effect waves-green btn red darken-1" data-dismiss="modal">
                            <i class="material-icons">clear</i>
                        </button>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center" aria-label="pagination">
        {{ $pages }}
    </div>

    @if(auth()->user()->is_admin)
        <div class="fixed-action-btn">
            <a href="{{ route('projects.create') }}" class="btn-floating waves-effect btn-large teal lighten-1">
                <i class="large material-icons">add</i>
            </a>
        </div>
    @endif
@endsection