<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sprint_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name');
            $table->enum('status', ['Backlog', 'In progress', 'Testing', 'Ready for review', 'Done']);
            $table->enum('complexity', ['0', '1/2', '2', '3', '5', '8', '13', '20', '40', '100']);
            $table->float('estimated_time', 4, 2);
            $table->enum('priority', ['Low', 'Medium', 'High']);
            $table->text('description');
            $table->timestamps();
            $table->foreign('sprint_id')->references('id')->on('sprints');
            $table->foreign('user_id')->references('id')->on('users');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign(['sprint_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('tasks');
    }
}
