<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    $status = ['Backlog', 'In progress', 'Testing', 'Ready for review', 'Done'];
    $complexities = ['0', '1/2', '2', '3', '5', '8', '13', '20', '40', '100'];
    $priorities = ['Low', 'Medium', 'High'];
    $created_at = $faker->dateTimeBetween('-2 years');
    return [
        'sprint_id' => $faker->randomElement(range(1, 30)),
        'name' => $faker->sentence,
        'status' => $status[array_rand($status)],
        'complexity' => $complexities[array_rand($complexities)],
        'estimated_time' => $faker->randomFloat(2, 0.5, 16),
        'priority' => $priorities[array_rand($priorities)],
        'description' => $faker->text(250),
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at)
    ];
});
