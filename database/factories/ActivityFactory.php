<?php

use Faker\Generator as Faker;

$factory->define(App\Activity::class, function (Faker $faker) {
    $created_at = $faker->dateTimeBetween('-2 years');
    return [
        'task_id' => $faker->randomElement(range(1, 200)),
        'description' => $faker->text(500),
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at)
    ];
});
