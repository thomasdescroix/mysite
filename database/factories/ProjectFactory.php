<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    $array = array('New', 'In Progress', 'Done');
    $created_at = $faker->dateTimeBetween('-2 years');
    return [
        'name' => $faker->unique()->word,
        'description' => $faker->text(600),
        'status' => $array[array_rand($array)],
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at)
    ];
});
