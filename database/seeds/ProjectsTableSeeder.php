<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create 20 App\Project instances using Factory
         */
        factory(App\Project::class, 20)->create();
    }
}
