<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SprintsTableSeeder extends Seeder
{
    /**
     * @return Carbon
     */
    private function randDate()
    {
        return Carbon::createFromDate(rand(2017, 2018), rand(1, 12), rand(1, 28));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; ++$i) {
            $numbers = rand(2, 6);
            for ($j = 1; $j < $numbers; ++$j) {
                $date = $this->randDate();
                DB::table('sprints')->insert([
                    'project_id' => $i,
                    'number' => $j,
                    'created_at' => $date,
                    'updated_at' => $date
                ]);
            }

        }

    }
}
