<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create 48 App\User instances using Factory
         */
        factory(App\User::class, 48)->create();

        DB::table('users')->insert([
            [
                'name' => 'user',
                'email' => 'user@user.com',
                'password' => bcrypt('user'),
                'avatar' => 'http://via.placeholder.com/150x150',
                'created_at' => date(now()),
                'updated_at' => date(now()),
                'is_admin' => false
            ],
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
                'avatar' => 'http://via.placeholder.com/150x150',
                'created_at' => date(now()),
                'updated_at' => date(now()),
                'is_admin' => true
            ],

        ]);
    }
}
