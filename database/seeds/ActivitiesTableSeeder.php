<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create 500 App\Activity instances using Factory
         */
        factory(App\Activity::class, 500)->create();
    }
}
