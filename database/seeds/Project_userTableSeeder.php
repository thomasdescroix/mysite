<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Project_userTableSeeder extends Seeder
{
    /**
     * @return Carbon
     */
    private function randDate()
    {
        return Carbon::createFromDate(rand(2017, 2018), rand(1, 12), rand(1, 28));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 50; ++$i) {
            $projects_id = range(1, 20);
            shuffle($projects_id);
            $n = rand(1, 4);
            for ($j = 1; $j < $n; ++$j) {
                $date = $this->randDate();
                DB::table('project_user')->insert([
                    'user_id' => $i,
                    'project_id' => $projects_id[$j],
                    'created_at' => $date,
                    'updated_at' => $date
                ]);
            }
        }
    }
}
