<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create 200 App\Task instances using Factory
         */
        factory(App\Task::class, 200)->create();
    }
}
