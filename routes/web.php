<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DashboardController@index')->name('dashboard');
Route::get('projects/{project}/users', 'TeamController@edit')->name('projects.users.edit');
Route::put('projects/{project}/users', 'TeamController@update')->name('projects.users.update');
Route::put('tasks/{task}/status/{status}', 'TaskController@updateStatus');

Route::resource('projects', 'ProjectController')->except('show');
Route::resource('projects.sprints', 'SprintController');
Route::resource('projects.sprints.tasks', 'TaskController');
Route::resource('projects.sprints.tasks.activities', 'ActivityController');

