<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sprint
 * @package App
 */
class Sprint extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_id', 'number'];

    /**
     * Get the project record associated with the sprint
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo*
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * Get the tasks for the sprint.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function tasksByStatus(String $status)
    {
        return $this->tasks->where('status', $status);
    }


}
