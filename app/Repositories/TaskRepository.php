<?php

namespace App\Repositories;

use App\Task;

/**
 * Class TaskRepository
 * @package App\Repositories
 */
class TaskRepository extends Repository
{
    /**
     * @var Task
     */
    protected $task;

    /**
     * TaskRepository constructor.
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->model = $task;
    }

    /**
     * @param int $taskId
     * @param String $status
     */
    public function updateStatus(int $taskId, String $status)
    {
        $task = $this->getById($taskId);
        $task->status = $status;
        $task->save();
    }
}
