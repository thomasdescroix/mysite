<?php

namespace App\Repositories;

/**
 * Class Repository use to implement the Pattern Repository
 * @package App\Repositories
 */
abstract class Repository implements IRepository
{
    protected $model;

    /**
     * Display a listing of records
     *
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Store a newly created object in storage
     *
     * @param array $inputs
     * @return mixed
     */
    public function store(Array $inputs)
    {
        return $this->model->create($inputs);
    }

    /**
     * Get the first result of the query or return an error
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Update the specified item in storage.
     *
     * @param int $id
     * @param array $inputs
     */
    public function update(int $id, Array $inputs)
    {
        dd($inputs);
        $this->getById($id)->update($inputs);
    }

    /**
     * Remove the specified item from storage.
     *
     * @param int $id
     */
    public function destroy(int $id)
    {
        $this->getById($id)->delete();
    }

    /**
     * Paginate a listing of records
     *
     * @param String $field
     * @param String $direction
     * @param int $nbPerPage
     * @return mixed
     */
    public function paginate(String $field = 'id', String $direction = 'asc', int $nbPerPage = 10)
    {
        return $this->model->orderBy($field, $direction)->paginate($nbPerPage);
    }

    /**
     * Display the total number of records
     *
     * @return mixed
     */
    public function count()
    {
        return $this->model->count();
    }
}