<?php

namespace App\Repositories;

/**
 * Interface RepositoryInterface
 * @package App\Repositories
 */
interface IRepository
{
    public function all();

    public function store(Array $inputs);

    public function getById(int $id);

    public function update(int $id, Array $inputs);

    public function destroy(int $id);

    public function paginate(String $field = 'id', String $direction = 'asc', int $nbPerPage = 10);

    public function count();
}
