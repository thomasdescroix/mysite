<?php

namespace App\Repositories;

use App\Sprint;

/**
 * Class SprintRepository
 * @package App\Repositories
 */
class SprintRepository extends Repository
{
    /**
     * @var Sprint
     */
    protected $sprint;

    /**
     * SprintRepository constructor.
     * @param Sprint $sprint
     */
    public function __construct(Sprint $sprint)
    {
        $this->model = $sprint;
    }
}
