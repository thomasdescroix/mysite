<?php

namespace App\Repositories;

use App\Project;

/**
 * Class ProjectRepository
 * @package App\Repositories
 */
class ProjectRepository extends Repository
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * ProjectRepository constructor.
     *
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->model = $project;
    }

    /**
     * Display a listing of records
     *
     * @param string $orderField
     * @param string $direction
     * @param int $nbPerPage
     * @return mixed
     */
    public function paginate(string $orderField = 'id', string $direction = 'asc', int $nbPerPage = 10)
    {
        return parent::paginate('name', 'asc', 10);
    }

    /**
     * Get all the projects in progress
     *
     * @return Project[]|\Illuminate\Database\Eloquent\Collection
     */
    public function allInProgress()
    {
        return $this->model->all()->where('status', 'In progress');
    }
}
