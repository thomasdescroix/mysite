<?php

namespace App\Repositories;

use App\Activity;

/**
 * Class ActivityRepository
 * @package App\Repositories
 */
class ActivityRepository extends Repository
{
    /**
     * @var Activity
     */
    protected $activity;

    /**
     * ActivityRepository constructor.
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->model = $activity;
    }
}
