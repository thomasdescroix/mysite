<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Activity
 * @package App
 */
class Activity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['task_id', 'description'];

    /**
     * Get the tasks record associated with the activity
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo*
     */
    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
