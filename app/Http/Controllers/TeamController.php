<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateTeamRequest;
use App\Project;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;

/**
 * Class TeamController
 * @package App\Http\Controllers
 */
class TeamController extends Controller
{
    /**
     * @var ProjectRepository
     */
    protected $projectRepository;
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * TeamController constructor.
     * @param ProjectRepository $projectRepository
     * @param UserRepository $userRepository
     */
    public function __construct(ProjectRepository $projectRepository, UserRepository $userRepository)
    {
        $this->middleware('admin');
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Show the form for editing the team of specified project.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        $team_members = $project->users()->allRelatedIds()->toArray();
        $users = $this->userRepository->all();

        return view('projects.teams.edit', compact('project', 'team_members', 'users'));
    }

    /**
     * Update the users of the specified project.
     *
     * @param UpdateTeamRequest $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateTeamRequest $request, Project $project)
    {
        $users = $request->input('users');
        $project->users()->sync($users);
        return redirect()->route('projects.index')->with('success', __('team.success.update'));
    }
}
