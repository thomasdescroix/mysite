<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Project;
use App\Repositories\ProjectRepository;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 */
class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * ProjectController constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => ['store', 'create', 'update', 'edit', 'destroy']]);
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of projects using pagination system.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = $this->projectRepository->paginate();
        $pages = $projects->render();
        return view('projects.index', compact('projects', 'pages'));
    }

    /**
     * Show the form for creating a new project.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created project in storage.
     *
     * @param StoreProjectRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreProjectRequest $request)
    {
        $this->projectRepository->store($request->all());
        return redirect()->route('projects.index')->with('success', __('project.success.store'));
    }

    /**
     * Show the form for editing the specified project.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified project in storage.
     *
     * @param UpdateProjectRequest $request
     * @param int $projectId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProjectRequest $request, int $projectId)
    {
        $this->projectRepository->update($projectId, $request->all());
        return redirect()->route('projects.index')->with('success', __('project.success.update'));
    }

    /**
     * Remove the specified project from storage.
     *
     * @param int $projectId
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $projectId)
    {
        $this->projectRepository->destroy($projectId);
        return back()->with('success', __('project.success.destroy'));
    }
}