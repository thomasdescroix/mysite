<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Repositories\TaskRepository;
use App\Task;
use App\Sprint;

class TaskController extends Controller
{
    /**
     * @var TaskRepository
     */
    protected $taskRepository;

    /**
     * ProjectController constructor.
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->middleware('auth');
        $this->middleware('team', ['only' => ['store', 'create', 'update', 'edit', 'destroy']]);
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Sprint $sprint
     * @return \Illuminate\Http\Response
     */
    public function index(Sprint $sprint)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Sprint $sprint
     * @return \Illuminate\Http\Response
     */
    public function create(Sprint $sprint)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTaskRequest $request
     * @param  \App\Sprint $sprint
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request, Sprint $sprint)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sprint $sprint
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Sprint $sprint, Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sprint $sprint
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(int $sprint, int $task)
    {
        dd('ok');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTaskRequest $request
     * @param  \App\Sprint $sprint
     * @param  \App\Task $task
     */
    public function update(UpdateTaskRequest $request, Sprint $sprint, Task $task)
    {
        //
    }

    /**
     * Update the status of the specified task in storage
     *
     * @param int $taskId
     * @param String $status
     */
    public function updateStatus(int $taskId, String $status)
    {
        // TODO: create API
        $this->taskRepository->updateStatus($taskId, $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sprint $sprint
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sprint $sprint, Task $task)
    {
        //
    }
}
