<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Http\Requests\StoreActivityRequest;
use App\Http\Requests\UpdateActivityRequest;
use App\Task;

/**
 * Class ActivityController
 * @package App\Http\Controllers
 */
class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function index(Task $task)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function create(Task $task)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreActivityRequest $request
     * @param  \App\Task $task
     * @return void
     */
    public function store(StoreActivityRequest $request, Task $task)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @param  \App\Activity $activity
     * @return void
     */
    public function show(Task $task, Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @param  \App\Activity $activity
     * @return void
     */
    public function edit(Task $task, Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateActivityRequest $request
     * @param  \App\Task $task
     * @param  \App\Activity $activity
     * @return void
     */
    public function update(UpdateActivityRequest $request, Task $task, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @param  \App\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task, Activity $activity)
    {
        //
    }
}
