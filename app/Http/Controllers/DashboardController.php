<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ProjectRepository;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @var ProjectRepository
     */
    protected $projectRepository;
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Create a new controller instance.
     *
     * DashboardController constructor.
     * @param UserRepository $userRepository
     * @param ProjectRepository $projectRepository
     */
    public function __construct(UserRepository $userRepository, ProjectRepository $projectRepository)
    {
        $this->middleware('auth');
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nbProjects = $this->projectRepository->count();
        $nbUsers = $this->userRepository->count();
        $teams = $this->projectRepository->allInProgress();

        return view('dashboard', compact('nbProjects', 'nbUsers', 'teams'));
    }
}
