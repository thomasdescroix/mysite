<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSprintRequest;
use App\Http\Requests\UpdateSprintRequest;
use App\Repositories\SprintRepository;
use App\Sprint;
use App\Project;

/**
 * Class SprintController
 * @package App\Http\Controllers
 */
class SprintController extends Controller
{
    /**
     * @var SprintRepository
     */
    protected $sprintRepository;

    /**
     * @var int
     */
    protected $paginate = 10;

    /**
     * @var array
     */
    protected $status = [
        'Backlog',
        'In progress',
        'Testing',
        'Ready for review',
        'Done'
    ];

    /**
     * SprintController constructor.
     * @param SprintRepository $sprintRepository
     */
    public function __construct(SprintRepository $sprintRepository)
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => 'destroy']);
        $this->middleware('team', ['only' => 'show']);
        $this->sprintRepository = $sprintRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Project $project)
    {
        return view('sprints.create', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreSprintRequest $request
     * @param  \App\Project $project
     */
    public function store(StoreSprintRequest $request, Project $project)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @param  \App\Sprint $sprint
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Sprint $sprint)
    {
        if ($project->sprint($sprint->id)) {
            $tasksByStatus = collect();
            foreach ($this->status as $s) {
                $tasksByStatus->put($s, $sprint->tasksByStatus($s));
            }
            return view('sprints.show', compact('project', 'sprint', 'tasksByStatus'));
        }
        return back()->with('warning', __('sprint.warning.show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @param  \App\Sprint $sprint
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Sprint $sprint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSprintRequest $request
     * @param  \App\Project $project
     * @param  \App\Sprint $sprint
     */
    public function update(UpdateSprintRequest $request, Project $project, Sprint $sprint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $sprintId
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $sprintId)
    {
        $this->sprintRepository->destroy($sprintId);
        return back()->with('success', __('sprint.success.destroy'));
    }
}
