<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateProjectRequest
 * @package App\Http\Requests
 */
class UpdateProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignoredId = $this->project;
        return [
            'name' => 'required|string|max:60|unique:projects,name,'.$ignoredId,
            'description' => 'required|string|max:600',
            'status' => 'required|in:New,In progress,Done'
        ];
    }
}
