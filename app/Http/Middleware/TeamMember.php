<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class Team
 * @package App\Http\Middleware
 */
class TeamMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->is_admin) {
            return $next($request);
        } elseif ($request->project->user($request->user()->id)) {
            return $next($request);
        }
        return back()->with('warning', __('middleware.warning.permission'));
    }
}
