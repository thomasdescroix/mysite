<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Project
 * @package App
 */
class Project extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'status'];

    /**
     * Get the sprints for the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sprints()
    {
        return $this->hasMany('App\Sprint');
    }

    /**
     * The Users that belong to the project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /**
     * Check if the user is a team member of the project
     *
     * @param int $user_id
     * @return bool
     */
    public function user(int $user_id)
    {
        return $this->users()->where('user_id', $user_id)->exists();
    }

    /**
     * Check if the sprint is a sprint of the project
     *
     * @param int $sprint_id
     * @return bool
     */
    public function sprint(int $sprint_id)
    {
        return $this->sprints()->where('id', $sprint_id)->exists();
    }
}
